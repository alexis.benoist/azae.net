

# Jekyll

Fichiers:
- _config.yml
- _data/
- _includes/
- _layouts/
- _plugins/
- _site/
- _src/
- about/
- articles/
- assets/
- contact.html
- contact.vcf
- devops.html
- Dockerfile
- feed.xml
- formation-ng.md
- Gemfile
- Gemfile.lock
- index.html
- README.md
- run
- robots.txt
- services/
- sitemap.xml


# TODO
- Contact
- contact.vcf generé avec interpolation
- Articles list
- Article layout
- devops page
- RSS
- robots.txt
- sitemap
- team
- about
- assets

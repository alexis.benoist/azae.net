---
title: Formations
description: >
    Nous pouvons réaliser n’importe quelle formation autour des domaines suivants: Les méthodes agiles (Scrum, Kanban, eXtrem Programming, Lean, ...), Devops, Software caftmanship (TDD, BDD, refactoring, clean code, ...), Technique (Docker, Java, Go, Puppet, Python), L’intelligence collective, Lean startup.
show_on_index:
  order: 3
  icon: fa-graduation-cap
  color: color-sky-blue
  summary: >
    Organisme de formation agréé, nos formations peuvent être prises en charge.
    
---

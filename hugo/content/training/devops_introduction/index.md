---
title: Devops
description: "Comprendre la philosophie Devops et son implication dans une organisation"
illustration: /assets/training/devops-introduction.png
category: peoples
tags: [methods, beginers]
presentation: |
objective: |
  - Détailler les divergeances stratégiques entre développeurs et opérationnels
  - Identifier les majques de communication entre ces équipes
  - Rapprocher les processus de déploiement et de mise en production
  - Industrialiser au-delà du développement
  - Comprendre le concept d'infrastructure as code
prerequists: |
  - Avoir participé à un projet informatique
  - Avoir de l'autonomie et de la motivation
plan: |
  - Initiative devops
  - Dev et ops
  - Rapprochement des équipes
  - Etapes d'une démarche devops
---

---
title: Introduction aux méthodes agiles
description: "Découvrir les méthodes agiles et en expérimenter des pratiques"
category: peoples
tags: [methods, beginers]
presentation: |
  Cette formation de deux jours a pour objectifs de faire découvrir les bases de l'agilité : sa philosophie, ses pratiques, la raison d'être et comment l'implémenter. Elle est destinée aux équipes IT qui souhaitente entamer ou poursuivre une transformation vers l'agilité. Cette introduction à l'agilité s'adapte aux différentes équipes et reste une occasion pour tous de découvrir, d'approfondir ou d'enrichir sa conaissance de l'agiité et des différents possibles.
  La pratique se fera sur vos projets mais aussi dans des ateliers de simulations ludiques par exemple avec des Lego(c).
objective: |
  - initiation aux pratiques agiles
  - comprendre et utiliser les principales méthodes agiles
prerequists: |
  - Avoir participé à un projet informatique
  - Avoir de l'autonomie et de la motivation
duration: 2 jours alternant théorie et *pratique sur vos projets*.
plan: |
  - Pourquoi l'agilité ?
  - Un peu d'histoire
  - Présentation générale
  - Les rôles
  - Définir les besoins
  - Gérer l'itération
  - Quelques bonnes pratiques
resources:
  - name: Plan de formation
    url: /assets/training/agile_introduction-plan.pdf
  - name: Support de cours
    url: /assets/training/agile_introduction-slides.pdf
  - name: Infographie des apprentissages
    url: /assets/training/agile_introduction-summary.png
---

---
name: Albiez Olivier
vcard_name: "Albiez;Olivier"
photo: albiez_olivier.jpg
mail: oalbiez@azae.net
mobile: "+33662767812"
cv: albiez_olivier.pdf
social:
  - url: https://github.com/oalbiez
    icon: fa-github
  - url: https://twitter.com/OlivierAlbiez
    icon: fa-twitter
  - url: https://www.linkedin.com/in/olivieralbiez
    icon: fa-linkedin
  - url: https://gitlab.com/oalbiez
    icon: fa-gitlab
bio: |
  Je suis artisan logiciel et coach agile chez [Azaé](http://azae.net), passionné des méthodes de travail et de l'auto-organisation dans les entreprises. Curieux par nature, je suis toujours à la recherche de ce qui peut améliorer le travail collectif d'une organisation. Je suis récemment co-fondateur de [Deliverous](http://deliverous.com). Je suis signataire des manifestes [Artisans du Logiciel](http://manifesto.softwarecraftsmanship.org/) et [Agile](http://agilemanifesto.org/).
outputs:
- vcard
---

---
name: Clavier Thomas
vcard_name: "Clavier;Thomas"
photo: clavier_thomas.jpg
mail: tclavier@azae.net
mobile: "+33620818130"
cv: clavier_thomas.pdf
social:
  - url: https://github.com/tclavier
    icon: fa-github
  - url: http://plus.google.com/+ThomasClavier
    icon: fa-google-plus
  - url: https://twitter.com/thomasclavier
    icon: fa-twitter
  - url: http://fr.linkedin.com/in/thomasclavier
    icon: fa-linkedin
  - url: http://fr.viadeo.com/fr/profile/thomas.clavier
    icon: fa-viadeo
bio: |
  Je suis coach agile chez [Azaé](http://azae.net), enseignant à l'université de Lille 1 et co-fondateur de [Deliverous](http://deliverous.com),
  mes sujets de prédilection sont l'agilité, devops, docker, le lean startup et l'artisanat
  logiciel. Depuis plus de 10 ans, j'essaye de transformer le travail en un jeu, faire progresser
  les geeks et challenger les managers, poser des questions pour faire progresser les équipes.
outputs:
- vcard
---

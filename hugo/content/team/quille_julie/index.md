---
name: Quille Julie
vcard_name: "Quille;Julie"
photo: quille_julie.jpg
mail: jquille@azae.net
mobile: "+33635589381"
cv: quille_julie.pdf
social:
bio: |
  Passionnée par l’être humain, la psychologie et la sociologie, j’accompagne les personnes et les équipes dans leur cheminement vers plus de responsabilité et de liberté individuelles. La Communication Bienveillante (CNV) est mon outil de prédilection.
  Dans un dynamique d'amélioration continue, il me tient à cœur d’élargir en permanence mes domaines de compétences, par des lectures, des recherches, des formations, mais surtout à travers des échanges et de l’expérimentation.
  Ce sont mes intérêts pour l’épanouissement au travail, l’adaptabilité et le travail collaboratif qui m’ont amené à l’agilité et au coaching d'équipes.
outputs:
- vcard
---
